package com.wecommit.walm.model;

import lombok.Data;

@Data
public class UserTokenState {
    private String tokenType = "Bearer";
    private String access_token;

    public UserTokenState() {
        this.access_token = null;
    }

    public UserTokenState(String access_token) {
        this.access_token = access_token;
    }
}