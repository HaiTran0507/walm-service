package com.wecommit.walm.model;

import lombok.Data;

import java.util.Date;

@Data
public class SignupModel {
    private Long userId;
    private String userName;
    private String password;
    private Date created;
    private String role;
}
