package com.wecommit.walm.service;

import com.wecommit.walm.entity.User;
import com.wecommit.walm.model.SignupModel;
import com.wecommit.walm.repository.UserRepository;
import com.wecommit.walm.security.auth.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository repo;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = repo.findByUserName(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new CustomUserDetails(user);
    }

    public UserDetails loadUserById(Long id) {
        User user = repo.findById(id).orElse(null);
        if (user == null) {
            throw new UsernameNotFoundException("Not found");
        }
        return new CustomUserDetails(user);
    }

    public boolean create(SignupModel model){
        if(repo.existsByUserName(model.getUserName())){
            return false;
        }
        User user=new User();
        user.setPassword(passwordEncoder.encode(model.getPassword()));
        user.setUserName(model.getUserName());
        user.setRole(model.getRole());
        repo.save(user);
        return true;
    }
    public boolean changePassword(String oldPassword, String newPassword) {
        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
        String username = currentUser.getName();
//
//        User user = repo.findById(id).orElse(null);
//        System.out.println("\n\n\n\n"+user.getPassword()+"\n\n\n"+passwordEncoder.encode(oldPassword));
//        if(user.getPassword().equals(passwordEncoder.encode(oldPassword))){
//            user.setPassword(passwordEncoder.encode(newPassword));
//            user.setCreated(new Date());
//            repo.save(user);
//            return true;
//        }else return false;
        if (authenticationManager != null) {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, oldPassword));
        } else {
            return false;
        }
        User user = repo.findByUserName(username);
        user.setPassword(passwordEncoder.encode(newPassword));
        repo.save(user);
        return true;
    }
    public void dedele(Long id){
        repo.deleteById(id);
    }

}