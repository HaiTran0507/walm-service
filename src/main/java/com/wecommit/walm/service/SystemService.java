package com.wecommit.walm.service;

import com.wecommit.walm.entity.System;
import com.wecommit.walm.repository.SystemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SystemService {
    @Autowired
    private SystemRepository repo;

    public Page<System> listAlert(Integer offset, Integer limit, String keyWord) {
        Pageable pageable = PageRequest.of(offset, limit);
        return repo.findByHostNameContaining(keyWord, pageable);
    }

    public void create(System system) {
        system.setCreated(new Date());
        repo.save(system);
    }

    public void delele(Long id) {
        repo.deleteById(id);
    }

    public System update(Long id, System system) {
        System entity = repo.findById(id).orElse(null);
        entity.setIp(system.getIp());
        entity.setHostName(system.getHostName());
        entity.setDbName(system.getDbName());
        repo.save(entity);
        return entity;
    }
}
