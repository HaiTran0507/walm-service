package com.wecommit.walm.service;

import com.wecommit.walm.entity.Alert;
import com.wecommit.walm.repository.AlertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@Service
public class AlertService {
    @Autowired
    private AlertRepository repo;

    public Page<Alert> listAlert(Integer offset, Integer limit, String keyWord) {
        Pageable pageable = PageRequest.of(offset, limit);

        return repo.findByNameContaining(keyWord, pageable);
    }

    public void create(Alert alert) {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        Date date = calendar.getTime();
        alert.setCreated(date);
        repo.save(alert);
    }

    public void delele(Long id) {
        repo.deleteById(id);
    }

    public Alert update(Long id, Alert alert) {
        Alert entity = repo.findById(id).orElse(null);
        entity.setName(alert.getName());
        entity.setDetail(alert.getDetail());
        entity.setSystemId(alert.getSystemId());
        entity.setStatus(alert.getStatus());
        entity.setSeverity(alert.getSeverity());
        entity.setNote(alert.getNote());
        repo.save(entity);
        return entity;
    }
}
