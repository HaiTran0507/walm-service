package com.wecommit.walm.service;

import com.wecommit.walm.entity.Customer;
import com.wecommit.walm.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository repo;

    public Page<Customer> listCustomer(Integer offset, Integer limit, String keyWord) {
        Pageable pageable = PageRequest.of(offset, limit);
        return repo.findByCustomerNameContaining(keyWord, pageable);
    }

    public void create(Customer customer) {
        customer.setCreated(new Date());
        repo.save(customer);
    }

    public void delele(Long id) {
        repo.deleteById(id);
    }

    public Customer update(Long id, Customer customer) {
        Customer entity = repo.findById(id).orElse(null);
        entity.setAddress(customer.getAddress());
        entity.setCustomerName(customer.getCustomerName());
        entity.setSystemId(customer.getSystemId());
        entity.setEmail(customer.getEmail());
        repo.save(entity);
        return entity;
    }
}
