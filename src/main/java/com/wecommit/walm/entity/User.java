package com.wecommit.walm.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String userName;
    private String password;
    private Date created;
    private String role;

    public User() {
    }

    public User(User user) {
        this.userName=user.getUserName();
        this.password=user.getPassword();
        this.created=user.getCreated();
        this.id=user.getId();
        this.role=user.getRole();
    }
}
