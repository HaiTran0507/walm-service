package com.wecommit.walm.controller;

import com.wecommit.walm.entity.System;
import com.wecommit.walm.service.SystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class SystemController {
    @Autowired
    private SystemService service;

    @GetMapping("/walm/system")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Page<System> listAlert(@RequestParam Integer offset, @RequestParam Integer limit, @RequestParam String keyWord) {
        return service.listAlert(offset, limit, keyWord);
    }

    @PostMapping("/walm/system/add")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN') or hasRole('MACHINE')")
    public void create(@RequestBody System system) {
        service.create(system);
    }

    @DeleteMapping("/walm/system/delete/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public void delele(@PathVariable Long id) {
        service.delele(id);
    }

    @PutMapping("/walm/system/update/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public System update(@PathVariable Long id, @RequestBody System system) {
        return service.update(id, system);
    }
}
