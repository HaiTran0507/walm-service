package com.wecommit.walm.controller;

import com.wecommit.walm.entity.Customer;
import com.wecommit.walm.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class CustomerController {
    @Autowired
    private CustomerService service;

    @GetMapping("/walm/customer")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    Page<Customer> listCustomer(@RequestParam Integer offset, @RequestParam Integer limit, @RequestParam String keyWord) {
        return service.listCustomer(offset, limit, keyWord);
    }

    @PostMapping("/walm/customer/add")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public void create(@RequestBody Customer customer) {
        service.create(customer);
    }

    @DeleteMapping("/walm/customer/delete/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public void delele(@PathVariable Long id) {
        service.delele(id);
    }

    @PutMapping("/walm/customer/update/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Customer update(@PathVariable Long id, @RequestBody Customer customer) {
        return service.update(id, customer);
    }
}
