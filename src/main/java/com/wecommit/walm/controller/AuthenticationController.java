package com.wecommit.walm.controller;

import com.wecommit.walm.entity.User;
import com.wecommit.walm.model.SignupModel;
import com.wecommit.walm.model.UserTokenState;
import com.wecommit.walm.repository.UserRepository;
import com.wecommit.walm.security.TokenHelper;
import com.wecommit.walm.security.auth.JwtAuthenticationRequest;
import com.wecommit.walm.service.CustomUserDetailsService;
import com.wecommit.walm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
public class AuthenticationController {

    @Autowired
   private TokenHelper tokenHelper;
    @Autowired private UserRepository user;
    @Lazy
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private CustomUserDetailsService userDetailsService;
     @Autowired private UserRepository repo;
    @Autowired private UserService service;


    @PostMapping("/auth/login")
    public ResponseEntity<?> createAuthenticationToken(
            @RequestBody JwtAuthenticationRequest authenticationRequest
    ) throws AuthenticationException, IOException {
        user.findByUserName("loda");
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getUsername(),
                        authenticationRequest.getPassword()
                )
        );

        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        final String token = tokenHelper.generateToken(userDetails);

        return ResponseEntity.ok(new UserTokenState(token));
    }

    @PostMapping("/auth/signup")
    public boolean create(@RequestBody SignupModel model){
        return service.create(model);
    }


    @PutMapping("/change-password")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN') or hasRole('MACHINE')")
    public boolean changePassword(@RequestBody PasswordChanger passwordChanger) {
        return service.changePassword(passwordChanger.oldPassword,passwordChanger.newPassword);
    }

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN') or hasRole('MACHINE')")
    public void delete(@PathVariable Long id){
        service.dedele(id);
    }

    static class PasswordChanger {
        public String oldPassword;
        public String newPassword;
    }
}