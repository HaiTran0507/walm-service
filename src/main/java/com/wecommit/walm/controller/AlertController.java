package com.wecommit.walm.controller;

import com.wecommit.walm.entity.Alert;
import com.wecommit.walm.service.AlertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class AlertController {
    @Autowired
    private AlertService service;

    @GetMapping("/walm/alert")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Page<Alert> listAlert(@RequestParam Integer offset, @RequestParam Integer limit, @RequestParam String keyWord) {

        return service.listAlert(offset, limit, keyWord);
    }

    @PostMapping("/walm/alert/add")
    @PreAuthorize("hasRole('MACHINE') or hasRole('ADMIN')")
    public void create(@RequestBody Alert alert) {
        service.create(alert);
    }

    @DeleteMapping("/walm/alert/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void delele(@PathVariable Long id) {
        service.delele(id);
    }

    @PutMapping("/walm/alert/update/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public Alert update(@PathVariable Long id, @RequestBody Alert alert) {
        return service.update(id, alert);
    }

}
