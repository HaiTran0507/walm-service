package com.wecommit.walm.repository;

import com.wecommit.walm.entity.Alert;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlertRepository extends JpaRepository<Alert,Long> {
    Page<Alert> findByNameContaining(String keyWord, Pageable pageable);
}
