package com.wecommit.walm.repository;

import com.wecommit.walm.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
    User findByUserName(String userName);
    Boolean existsByUserName(String username);
}
