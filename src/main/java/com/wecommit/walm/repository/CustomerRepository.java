package com.wecommit.walm.repository;

import com.wecommit.walm.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer,Long> {
    Page<Customer> findByCustomerNameContaining(String name, Pageable pageable);
}
