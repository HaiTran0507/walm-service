package com.wecommit.walm.repository;

import com.wecommit.walm.entity.System;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SystemRepository extends JpaRepository<System,Long> {
    Page<System> findByHostNameContaining(String name, Pageable pageable);
}
