
CREATE DATABASE IF NOT EXISTS `walm`;
USE `walm`;

CREATE TABLE IF NOT EXISTS `alert` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id của báo cáo',
  `system_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(1000) NOT NULL COMMENT 'tên lỗi',
  `severity` set('Critical','High','Medium','Low') NOT NULL COMMENT 'mức độ nghiêm trọng của lỗi',
  `detail` varchar(2000) NOT NULL COMMENT 'thông tin chi tiết lỗi',
  `status` bit(1) NOT NULL,
  `note` varchar(2500) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_alert_system` (`system_id`),
  CONSTRAINT `FK_alert_system` FOREIGN KEY (`system_id`) REFERENCES `system` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='thông tin về báo cáo';


INSERT INTO `alert` (`id`, `system_id`, `name`, `severity`, `detail`, `status`, `note`, `created`, `modified`) VALUES
	(1, 1, 'dasua', 'Medium', 'a', b'1', 'abcxxx', '2019-10-31 16:24:44', '2019-10-31 16:24:45'),
	(2, 1, 'acc', 'Medium', 'ac', b'0', 'abc', '2019-10-31 16:44:56', '2019-10-31 16:44:56');

CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_id` int(11) NOT NULL,
  `customer_name` varchar(500) NOT NULL,
  `address` varchar(500) NOT NULL,
  `email` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_customer_system` (`system_id`),
  CONSTRAINT `FK_customer_system` FOREIGN KEY (`system_id`) REFERENCES `system` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='thông tin khách hàng';


INSERT INTO `customer` (`id`, `system_id`, `customer_name`, `address`, `email`, `created`, `modified`) VALUES
	(2, 1, 'ab', 'abc', 'hai0507@gmail.com', '2019-10-31 16:54:28', '2019-10-31 16:54:28');

CREATE TABLE IF NOT EXISTS `system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host_name` varchar(500) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `db_name` varchar(500) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='thông tin về hệ thống';


INSERT INTO `system` (`id`, `host_name`, `ip`, `db_name`, `created`, `modified`) VALUES
	(1, '20', '120', 'namex', '2019-10-31 15:38:33', '2019-10-31 15:38:34');

CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` set('ADMIN','MACHINE','USER') COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `user` (`id`, `user_name`, `role`, `password`, `created`, `modified`) VALUES
	(8, 'loda', 'ADMIN', '$2a$10$DAZoA3REZ3VfIXhvV8Zv/eaqTJHM5K4WzQXmoFLUTN8LpDcLwPPlK', '2019-10-30 11:09:04', '2019-11-07 17:03:28'),
	(12, 'admin', 'ADMIN', '$2a$10$A/qPOi4MFXn5u8hfJS8HBeYuU.qPKznwyWhFf0WFq4QokeambLrNC', '2019-11-07 16:20:27', '2019-11-07 16:20:28'),
	(13, 'user', 'USER', '$2a$10$DEc6B3LZf0jv10eyuea4YOwXhVfkdoj93h0iB4Ao.FhrZgS2GOuX.', '2019-11-07 16:55:18', '2019-11-07 16:55:18'),
	(14, 'machine', 'MACHINE', '$2a$10$a/Ynj/7oG1TZ/YcswjE/Nuea7oj3RavLcCvPBhyTtoZgarUlFwuSi', '2019-11-07 16:55:19', '2019-11-07 16:55:19');

